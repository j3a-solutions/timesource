# Timesource

Event sourcing with [Timescaledb](https://www.timescale.com/) as storage engine for Rust applications.

See the [main crate README](`packages/timesource/README.md`)