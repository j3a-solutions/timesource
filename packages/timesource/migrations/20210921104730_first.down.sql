DROP FUNCTION append_to_store;

DROP TABLE aggregate_consumer;
DROP TABLE event;
DROP TABLE event_meta;
DROP TABLE aggregate_type;

DROP FUNCTION event_meta_id;
DROP FUNCTION aggregate_type_id;
DROP FUNCTION consumer_offset;
DROP FUNCTION unix_now;
DROP FUNCTION try_consumer_checkout;

DROP TYPE append_event_data;
DROP TYPE payload_encoding;

DROP EXTENSION timescaledb;
