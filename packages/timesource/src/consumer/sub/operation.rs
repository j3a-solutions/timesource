use crate::error::{Error, Result};
use backoff::ExponentialBackoff;
use futures::StreamExt;
use timesource_core::Persisted;
use tokio::sync::mpsc;

use super::super::{notification::EventNotification, store::ConsumerStore};

fn is_error_recoverable(error: &Error) -> bool {
    if let Error::Sqlx(error) = error {
        matches!(
            error,
            sqlx::Error::Database(_)
                | sqlx::Error::Io(_)
                | sqlx::Error::Tls(_)
                | sqlx::Error::PoolTimedOut
                | sqlx::Error::PoolClosed
                | sqlx::Error::WorkerCrashed
        )
    } else {
        false
    }
}

fn is_already_processed(last_known_offset: Option<u64>, event_id: u64) -> bool {
    last_known_offset
        .map(|last_known_offset| event_id <= last_known_offset)
        .unwrap_or_default()
}

//
// Check if consumer has received a notification with an event referring to some other missing one
// After consumer has tracked some events, it's possible that some go missing (e.g.: network error)
//
fn has_gaps_in_series(last_known_offset: Option<u64>, prior_event_id: Option<u64>) -> bool {
    prior_event_id
        .zip(last_known_offset)
        .map(|(prior_event_id, last_known_offset)| prior_event_id > last_known_offset)
        .unwrap_or_default()
}

#[derive(Debug)]
#[cfg_attr(test, derive(PartialEq))]
pub enum ConsumerOperation<T> {
    One(EventNotification<T>),
    Rewind(u64, EventNotification<T>),
    FromCommittedOffset,
    FromOffset(u64),
}

impl<T> ConsumerOperation<T>
where
    T: std::fmt::Debug + Send + 'static,
{
    pub fn from_notification(
        notification: EventNotification<T>,
        last_known_offset: Option<u64>,
    ) -> Option<Self> {
        let prior_event_id = notification.prior_event_id;
        let already_processed = is_already_processed(last_known_offset, notification.event.id());
        let has_gaps = has_gaps_in_series(last_known_offset, prior_event_id);

        if already_processed {
            warn!(
                last_known_offset,
                ?notification,
                "Received postgres notification for events which have already been processed"
            );
            None
        } else if has_gaps {
            error!(
                last_known_offset,
                ?notification,
                "It seems some postgres notifications went missing. Rewinding and catching up."
            );
            Some(ConsumerOperation::Rewind(
                last_known_offset.unwrap(),
                notification,
            ))
        } else {
            Some(ConsumerOperation::One(notification))
        }
    }

    pub async fn handle<Store>(
        self,
        store: &Store,
        sink: mpsc::Sender<Result<Persisted<T>>>,
    ) -> std::result::Result<Option<u64>, Option<u64>>
    where
        Store: ConsumerStore<Event = T>,
    {
        let last_event_id = if let ConsumerOperation::One(notification) = self {
            let event = notification.event;
            let event_id = event.id();
            if let Err(error) = sink.send(Ok(event)).await {
                error!(?error, "Sink dropped");
                return Ok(None);
            }
            Some(event_id)
        } else {
            backoff::future::retry(ExponentialBackoff::default(), || {
                self.retry_stream(store, sink.clone())
            })
            .await?
        };

        debug!("No more events to process for now");

        Ok(last_event_id)
    }

    async fn retry_stream<Store>(
        &self,
        store: &Store,
        sink: mpsc::Sender<Result<Persisted<T>>>,
    ) -> std::result::Result<Option<u64>, backoff::Error<Option<u64>>>
    where
        Store: ConsumerStore<Event = T>,
    {
        let mut stream = match self {
            ConsumerOperation::FromCommittedOffset => store.events_after_offset(),
            ConsumerOperation::FromOffset(last_known_offset) => {
                store.events_after(*last_known_offset)
            }
            ConsumerOperation::Rewind(offset, notification) => {
                store.events_range(*offset, notification.event.id())
            }
            ConsumerOperation::One(_) => {
                unreachable!()
            }
        };

        let mut last_event_id = None;

        while let Some(result) = stream.next().await {
            match result {
                Ok(event) => {
                    last_event_id = Some(event.id());
                    if let Err(error) = sink.send(Ok(event)).await {
                        error!(?error, "Sink dropped");
                        return Err(backoff::Error::Permanent(None));
                    }
                }
                Err(error) => {
                    // if last_event_id is some, don't repeat the operation, or else events may get delivered more than once
                    let backoff_err = if !is_error_recoverable(&error) || last_event_id.is_some() {
                        error!(
                            ?error,
                            "Unable to complete consumer operation. Skipping event"
                        );

                        if let Err(error) = sink.send(Err(error)).await {
                            error!(?error, "Sink dropped");
                        }

                        backoff::Error::Permanent(last_event_id)
                    } else {
                        warn!(
                            ?error,
                            "Recoverable error detected when consuming stream. Retrying"
                        );
                        backoff::Error::transient(last_event_id)
                    };
                    return Err(backoff_err);
                }
            }
        }

        Ok(last_event_id)
    }
}

impl<T> From<Option<u64>> for ConsumerOperation<T> {
    fn from(offset: Option<u64>) -> Self {
        match offset {
            Some(offset) => ConsumerOperation::FromOffset(offset),
            None => ConsumerOperation::FromCommittedOffset,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::super::super::test_util::{mocks, MockEvent};
    use super::*;
    use chrono::Utc;
    use futures::stream;
    use mockall::predicate;
    use test_case::test_case;
    use uuid::Uuid;

    //
    // NO-OP
    //

    #[test]
    fn given_notification_for_event_already_processed_returns_noop() {
        let last_known_offset = Some(1);

        let event = MockEvent::SomeEvent1;
        let notification = EventNotification {
            prior_event_id: Some(0),
            event: Persisted::new(Uuid::new_v4(), event, 1, Utc::now().timestamp_nanos()),
        };
        let op = ConsumerOperation::from_notification(notification, last_known_offset);

        assert!(op.is_none());
    }

    //
    // FROM OFFSET
    //

    #[test_case(None; "without offset")]
    #[test_case(Some(1_u64); "with offset")]
    fn given_known_offset_returns_op(offset: Option<u64>) {
        if let Some(known_offset) = offset {
            assert_eq!(
                ConsumerOperation::<MockEvent>::from(offset),
                ConsumerOperation::<MockEvent>::FromOffset(known_offset)
            )
        } else {
            assert_eq!(
                ConsumerOperation::<MockEvent>::from(offset),
                ConsumerOperation::<MockEvent>::FromCommittedOffset
            )
        }
    }

    #[allow(non_snake_case)]
    #[tokio::test]
    async fn given_FromOffset_operation_then_returns_events_after_known_offset() {
        let op = ConsumerOperation::FromOffset(1);
        let mut store = mocks::MockStore::new();
        let (tx, mut rx) = mpsc::channel(1);
        let event = MockEvent::SomeEvent1;
        let persisted = event.new_persisted(Uuid::new_v4(), 1);
        let expected = persisted.clone();
        store.expect_events_after_offset().times(0);
        store
            .expect_events_after()
            .with(predicate::eq(1))
            .times(1)
            .return_once(|_| stream::iter(vec![Ok(persisted)]).boxed());
        store.expect_events_range().times(0);

        op.handle(&store, tx).await.unwrap();

        let output = rx.recv().await.unwrap().unwrap();
        assert_eq!(output.id(), expected.id());
    }

    #[allow(non_snake_case)]
    #[tokio::test]
    async fn given_FromCommittedOffset_operation_then_returns_events_after_offset() {
        let op = ConsumerOperation::FromCommittedOffset;
        let mut store = mocks::MockStore::new();
        let (tx, mut rx) = mpsc::channel(1);
        let event = MockEvent::SomeEvent1;
        let persisted = event.new_persisted(Uuid::new_v4(), 0);
        let expected = persisted.clone();
        store
            .expect_events_after_offset()
            .times(1)
            .return_once(|| stream::iter(vec![Ok(persisted)]).boxed());
        store.expect_events_after().times(0);
        store.expect_events_range().times(0);

        op.handle(&store, tx).await.unwrap();

        let output = rx.recv().await.unwrap().unwrap();
        assert_eq!(output.id(), expected.id());
    }

    //
    // REWIND
    //

    #[test]
    fn given_gap_in_series_returns_rewind() {
        let last_known_offset = Some(2);
        let event = MockEvent::SomeEvent1;
        let notification = EventNotification {
            prior_event_id: Some(3),
            event: Persisted::new(Uuid::new_v4(), event, 4, Utc::now().timestamp_nanos()),
        };
        let op = ConsumerOperation::from_notification(notification.clone(), last_known_offset);
        assert_eq!(op, Some(ConsumerOperation::Rewind(2, notification)));
    }

    #[tokio::test]
    async fn given_rewind_operation_then_returns_events_range() {
        let event = MockEvent::SomeEvent1;
        let last_known_offset = 1;
        let persisted = event.new_persisted(Uuid::new_v4(), 3);
        let expected = persisted.clone();

        let notification = EventNotification {
            prior_event_id: Some(2),
            event: persisted.clone(),
        };
        let op = ConsumerOperation::Rewind(last_known_offset, notification);
        let mut store = mocks::MockStore::new();
        let (tx, mut rx) = mpsc::channel(1);

        store.expect_events_after_offset().times(0);
        store.expect_events_after().times(0);
        store
            .expect_events_range()
            .with(
                predicate::eq(last_known_offset),
                predicate::eq(persisted.id()),
            )
            .times(1)
            .return_once(|_, _| stream::iter(vec![Ok(persisted)]).boxed());

        op.handle(&store, tx).await.unwrap();

        let output = rx.recv().await.unwrap().unwrap();
        assert_eq!(output.id(), expected.id());
    }

    //
    // ONE
    //

    #[test]
    fn given_expected_notification_passes_payload() {
        let last_known_offset = Some(2);
        let event = MockEvent::SomeEvent1;
        let notification = EventNotification {
            prior_event_id: Some(2),
            event: Persisted::new(Uuid::new_v4(), event, 3, Utc::now().timestamp_nanos()),
        };
        let op = ConsumerOperation::from_notification(notification.clone(), last_known_offset);
        assert_eq!(op, Some(ConsumerOperation::One(notification)));
    }

    #[allow(non_snake_case)]
    #[tokio::test]
    async fn given_One_operation_then_returns_notification_event() {
        let event = MockEvent::SomeEvent1;
        let persisted = event.new_persisted(Uuid::new_v4(), 0);
        let expected = persisted.clone();

        let notification = EventNotification {
            prior_event_id: None,
            event: persisted,
        };
        let op = ConsumerOperation::One(notification);
        let mut store = mocks::MockStore::new();
        let (tx, mut rx) = mpsc::channel(1);

        store.expect_events_after_offset().times(0);
        store.expect_events_after().times(0);
        store.expect_events_range().times(0);

        op.handle(&store, tx).await.unwrap();

        let output = rx.recv().await.unwrap().unwrap();
        assert_eq!(output.id(), expected.id());
    }
}
