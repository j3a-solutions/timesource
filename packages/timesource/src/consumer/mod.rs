//! Contains a persisted implementation of the [`Consumer`] trait
//! using Postgres as the backend data source for its state.
//!
//! [`Consumer`]: ../../eventually-core/consumer/trait.Consumer.html

mod listener;
mod notification;
pub mod store;
mod sub;
#[cfg(test)]
mod test_util;

pub use self::store::aggregate::*;
pub use self::store::aggregate_root::*;
pub use self::sub::persistent::{Consumer, ConsumerBuilder};
pub use self::sub::transient::{TransientConsumer, TransientConsumerBuilder};
