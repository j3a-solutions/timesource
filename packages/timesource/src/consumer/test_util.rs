use super::store::{ConsumerStore, StoreData};
use crate::TimesourceEvent;
use crate::{self as timesource};
use chrono::Utc;
use futures::stream::BoxStream;

use serde::{Deserialize, Serialize};
use timesource_core::event::EventRow;
use timesource_core::{Persisted, TimesourceEventPayload};
use uuid::Uuid;

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq, Eq, TimesourceEvent)]
pub enum MockEvent {
    SomeEvent1,
    SomeEvent2,
}

impl MockEvent {
    pub fn new_persisted(&self, aggregate_id: Uuid, id: i64) -> Persisted<MockEvent> {
        let row = EventRow {
            aggregate_id,
            bytes: None,
            id,
            json: Some(serde_json::to_string(self).unwrap()),
            name: Self::name().to_string(),
            time: Utc::now().timestamp_nanos(),
        };

        Persisted::try_from(row).unwrap()
    }
}

pub mod mocks {
    use super::*;
    use mockall::mock;
    use mockall::predicate::*;

    mock! {
      #[derive(Debug)]
      pub Store {}

      impl Clone for Store {
          fn clone(&self) -> Self {
              Store {}
          }
      }

      impl ConsumerStore for Store
          {
              type Event = MockEvent;

              fn events_after_offset(&self) -> BoxStream<'_, StoreData<MockEvent>>;
              fn events_after(&self, offset: u64) -> BoxStream<'_, StoreData<MockEvent>>;
              fn events_range(&self, later_than: u64, until: u64) -> BoxStream<'_, StoreData<MockEvent>>;
          }
    }
}
