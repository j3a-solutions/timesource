use super::notification::EventNotification;
use crate::error::{Error, Result};
use sqlx::postgres::PgListener;
use sqlx::PgPool;
use std::borrow::Cow;
use std::fmt::Debug;
use timesource_core::TimesourceEventPayload;
use tokio::sync::mpsc;

#[derive(Debug)]
pub struct Listener {
    buffer_capacity: usize,
    channel: Cow<'static, str>,
    pool: PgPool,
}

impl Listener {
    pub fn new(buffer_capacity: usize, channel: Cow<'static, str>, pool: PgPool) -> Self {
        Self {
            buffer_capacity,
            channel,
            pool,
        }
    }

    /// PgListener is an unbounded mpsc channel
    /// This listener:
    ///   - transforms payload
    ///   - notifies when connection is disrupted
    ///   - handles backpressure
    pub async fn listen<T>(&self) -> Result<mpsc::Receiver<Result<EventNotification<T>>>>
    where
        T: std::fmt::Debug + TimesourceEventPayload + 'static + Send,
    {
        let channel = self.channel.as_ref();
        let (sink_tx, sink_rx) = mpsc::channel(self.buffer_capacity);

        let mut listener = PgListener::connect_with(&self.pool).await?;
        listener.listen(channel).await?;
        info!(channel, "Listening for notifications");
        let total_capacity = self.buffer_capacity;

        tokio::spawn(async move {
            loop {
                let message = match listener.try_recv().await {
                    Ok(Some(notification)) => {
                        debug!(?notification, "Received LISTEN notification");

                        EventNotification::try_from(notification.payload()).map_err(|error| {
                            error!(?error, ?notification, "Unable to decode event notification");
                            Error::from(error)
                        })
                    }
                    Ok(None) => Err(Error::ListenerDisconnected),
                    Err(error) => Err(Error::from(error)),
                };

                if let Err(error) = sink_tx.try_send(message) {
                    match error {
                        mpsc::error::TrySendError::Full(message) => {
                            error!(
                                total_capacity,
                                "Postgres notification channel is full. Applying backpressure."
                            );
                            if let Err(error) = sink_tx.send(message).await {
                                error!(?error, "All push notification receivers have been dropped");
                                break;
                            }
                        }
                        mpsc::error::TrySendError::Closed(_) => {
                            error!(?error, "All push notification receivers have been dropped");
                            break;
                        }
                    }
                }

                let remaining_capacity = sink_tx.capacity();
                let percentage_used = ((total_capacity - remaining_capacity) as f32 * 100_f32)
                    / total_capacity as f32;

                if percentage_used > 75.00 {
                    warn!(
                        total_capacity,
                        remaining_capacity,
                        percentage_used = ?format!("{:.2}", percentage_used),
                        "Congestion building up in postgres notification channel"
                    );
                }
            }
        });

        Ok(sink_rx)
    }
}
