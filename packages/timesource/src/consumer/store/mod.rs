pub mod aggregate;
pub mod aggregate_root;
pub mod checkpoint;

use crate::error::Result;
use crate::TimesourceEventPayload;
use futures::stream::BoxStream;
use timesource_core::event::Persisted;

pub type StoreData<Event> = Result<Persisted<Event>>;

pub trait ConsumerStore: Clone + Send + Sync + 'static + std::fmt::Debug {
    type Event: TimesourceEventPayload + Send + Sync + std::fmt::Debug;

    fn events_after_offset(&self) -> BoxStream<'_, StoreData<Self::Event>>;

    fn events_after(&self, offset: u64) -> BoxStream<'_, StoreData<Self::Event>>;

    fn events_range(&self, later_than: u64, until: u64) -> BoxStream<'_, StoreData<Self::Event>>;
}

#[async_trait]
pub trait ConsumerAck: std::fmt::Debug {
    async fn save_offset(&self, offset: u64) -> Result<()>;

    async fn try_save_offset(&self, offset: u64) -> Result<()>;
}
