pub type Result<T> = std::result::Result<T, Error>;

#[derive(Debug, thiserror::Error)]
pub enum Error {
    #[error("invalid data: {0}")]
    InvalidData(String),

    #[error("unable to parse event: ${0}")]
    PersistedError(#[from] timesource_core::PersistedError),

    #[error("store's sqlx client returned an error: ${0}")]
    Sqlx(#[from] sqlx::Error),

    #[error("Unable to perform SQL migration: ${0}")]
    MigrateError(#[from] sqlx::migrate::MigrateError),

    #[error("postgres LISTEN connection interrupted")]
    ListenerDisconnected,

    #[error("Aggregate not found")]
    AggregateRootNotFound,

    #[error("Aggregate error: ${0}")]
    Aggregate(#[from] Box<dyn std::error::Error + Send + Sync + 'static>),
}
