pub mod aggregate;
pub mod consumer;
pub mod error;
pub mod repository;
pub mod store;

#[macro_use]
extern crate async_trait;

#[macro_use]
extern crate tracing;

extern crate timesource_derive;

use timesource_core::event;

pub use aggregate::{Aggregate, AggregateRoot};
pub use event::{
    EventPayloadEncoding, PayloadEncoding, Persisted, PersistedError, TimesourceEventPayload,
};

#[doc(hidden)]
pub use timesource_derive::*;
