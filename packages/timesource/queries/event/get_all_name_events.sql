SELECT 
  n.id,
  n.name
FROM event_meta n
WHERE n.aggregate_type_id = $1
-- get most recently created
ORDER BY n.id DESC
-- cache size
LIMIT $2;