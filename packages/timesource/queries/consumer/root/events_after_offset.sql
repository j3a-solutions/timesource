SELECT
  e.aggregate_id,
  n.name,
  e.json::TEXT,
  e.bytes,
  e.time,
  e.id
FROM event e
  INNER JOIN aggregate_consumer s
    ON
      s.name = $3 AND
      e.aggregate_type_id = s.aggregate_type_id AND
      e.id > coalesce(s."offset", -9223372036854775808)
LEFT JOIN event_meta n
  ON e.meta_id = n.id
WHERE
  e.aggregate_type_id = $1 AND
  e.aggregate_id = $2
ORDER BY e.id;