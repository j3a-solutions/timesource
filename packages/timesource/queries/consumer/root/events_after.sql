SELECT
  e.aggregate_id,
  n.name,
  e.json::TEXT,
  e.bytes,
  e.time,
  e.id
FROM event e
LEFT JOIN event_meta n
  ON e.meta_id = n.id
WHERE
  e.aggregate_type_id = $1 AND
  e.id > $2 AND
  e.aggregate_id = $3
ORDER BY e.id;