use chrono::{DateTime, TimeZone, Utc};
use sqlx::FromRow;
use uuid::{Error as UuidError, Uuid};

/// An [`Event`](EventStore::Event) wrapper for events that have been
/// successfully committed to the [`EventStore`].
///
/// [`EventStream`]s are composed of these events.
#[derive(Debug, Clone, PartialEq)]
pub struct Persisted<T> {
    aggregate_id: Uuid,
    data: T,
    id: u64,
    time: i64,
}

impl<T> Persisted<T> {
    pub fn new(aggregate_id: Uuid, data: T, id: u64, time: i64) -> Self {
        Self {
            aggregate_id,
            data,
            id,
            time,
        }
    }

    /// Returns the event id
    pub fn id(&self) -> u64 {
        self.id
    }

    /// Returns the aggregate id.
    pub fn aggregate_id(&self) -> Uuid {
        self.aggregate_id
    }

    pub fn timestamp(&self) -> i64 {
        self.time
    }

    pub fn utc(&self) -> DateTime<Utc> {
        Utc.timestamp_nanos(self.time)
    }

    pub fn data(&self) -> &T {
        &self.data
    }

    pub fn into_data(self) -> T {
        self.data
    }
}

#[derive(sqlx::Type, Debug)]
#[sqlx(type_name = "payload_encoding", rename_all = "lowercase")]
pub enum PayloadEncoding {
    Json,
    Cbor,
    ProtoBuf,
}

#[derive(Debug)]
pub enum EventPayloadEncoding {
    Json(String),
    Bytes(Vec<u8>),
}

pub trait TimesourceEventPayload: Sized {
    fn name() -> &'static str;

    fn encoding() -> PayloadEncoding;

    fn version() -> Option<&'static str>;

    fn encode_to_payload(&self) -> Result<EventPayloadEncoding, PersistedError>;

    fn decode_from_payload(
        json: Option<&str>,
        bytes: Option<&[u8]>,
    ) -> Result<Self, PersistedError>;
}

#[derive(Debug, FromRow)]
pub struct EventRow {
    pub aggregate_id: Uuid,
    pub bytes: Option<Vec<u8>>,
    pub id: i64,
    pub json: Option<String>,
    pub name: String,
    pub time: i64,
}

impl<T> TryFrom<EventRow> for Persisted<T>
where
    T: TimesourceEventPayload,
{
    type Error = PersistedError;

    fn try_from(row: EventRow) -> Result<Self, Self::Error> {
        let event = Persisted {
            aggregate_id: row.aggregate_id,
            time: row.time,
            id: row.id as _,
            data: T::decode_from_payload(row.json.as_deref(), row.bytes.as_deref())?,
        };

        Ok(event)
    }
}

#[derive(Debug, thiserror::Error)]
pub enum PersistedError {
    #[cfg(feature = "data-encoding")]
    #[error("failed to decode bytes : ${0}")]
    Base64DecodeError(#[from] data_encoding::DecodeError),

    #[cfg(feature = "json")]
    #[error("failed to encode/decode json payload: ${0}")]
    SerdeJson(#[from] serde_json::Error),

    #[cfg(feature = "cbor")]
    #[error("failed to encode cbor payload: ${0}")]
    MiniCborEncode(String),

    #[cfg(feature = "cbor")]
    #[error("failed to decode cbor payload: ${0}")]
    MiniCborDecode(#[from] minicbor::decode::Error),

    #[cfg(feature = "protobuf")]
    #[error("failed to encode protobuf payload: ${0}")]
    ProstEncode(#[from] prost::EncodeError),

    #[cfg(feature = "protobuf")]
    #[error("failed to decode protobuf payload: ${0}")]
    ProstDecode(#[from] prost::DecodeError),

    #[cfg(feature = "json")]
    #[error("unable to decode event without JSON")]
    JsonMissing,

    #[cfg(feature = "data-encoding")]
    #[error("unable to decode event without Bytes")]
    BytesMissing,

    #[error("unable to parse value from &str: ${0}")]
    ParseIntError(#[from] std::num::ParseIntError),

    #[error("unable to parse into Uuid: ${0}")]
    ParseUuid(#[from] UuidError),
}
