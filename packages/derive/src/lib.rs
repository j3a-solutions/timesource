mod encoding;
mod meta;

#[macro_use]
extern crate quote;

use meta::EventMeta;
use proc_macro::TokenStream;
use syn::{parse_macro_input, DeriveInput, Error};

#[proc_macro_derive(TimesourceEvent, attributes(timesource))]
pub fn timesource_event(input: TokenStream) -> TokenStream {
    let mut item = parse_macro_input!(input as DeriveInput);

    let timesource_attr = item
        .attrs
        .iter()
        .find(|attr| attr.path.is_ident("timesource"));

    let event_meta = EventMeta::try_from(timesource_attr).unwrap();
    let ident = &item.ident;
    let name = ident.to_string();

    let (_impl_generics, ty_generics, where_clause) = item.generics.split_for_impl();

    let output = match &mut item.data {
        syn::Data::Struct(_) | syn::Data::Enum(_) => {
            quote! {
                impl #ty_generics timesource::TimesourceEventPayload for #ident #ty_generics #where_clause {
                    fn name() -> &'static str {
                        #name
                    }

                    #event_meta
                }
            }
        }
        syn::Data::Union(u) => {
            return Error::new_spanned(u.union_token, "Unions not supported for TimesourceEvent")
                .to_compile_error()
                .into()
        }
    };

    // println!("output = {:#}", output);

    output.into()
}
